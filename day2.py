with open("day2_input.txt", "r") as file:
    data = file.read().splitlines()

    # print(data)
    # hand_points = 0
    # skirmish_points = 0
    # for skirmish in data:
    #     match skirmish[2]:
    #         case "X":
    #             hand_points += 1
    #         case "Y":
    #             hand_points += 2
    #         case "Z":
    #             hand_points += 3
    #
    #     result = 0
    #
    #     if (skirmish[0] == "A" and skirmish[2] == "X")\
    #             or (skirmish[0] == "B" and skirmish[2] == "Y")\
    #             or (skirmish[0] == "C" and skirmish[2] == "Z"):
    #         result = 1
    #     elif (skirmish[0] == "A" and skirmish[2] == "Y")\
    #             or (skirmish[0] == "B" and skirmish[2] == "Z")\
    #             or (skirmish[0] == "C" and skirmish[2] == "X"):
    #         result = 2
    #
    #     if result == 1:
    #         skirmish_points += 3
    #     elif result == 2:
    #         skirmish_points += 6
    #
    # print(f"Hand points: {hand_points}")
    # print(f"Skirmish result: {skirmish_points}")
    # total = hand_points + skirmish_points
    # print(f"Total: {total}")



## NEXT PART

# X - Lose
# Y - Draw
# Z - Win
    new_hand_points = 0
    new_skirmish_points = 0

    for skirmish in data:
        match skirmish[2]:
            case "X":
                if skirmish[0] == "A":
                    new_hand_points += 3
                    print(new_hand_points)
                elif skirmish[0] == "B":
                    new_hand_points += 1
                    print(new_hand_points)
                elif skirmish[0] == "C":
                    new_hand_points += 2
                    print(new_hand_points)
            case "Y":
                new_skirmish_points += 3
                if skirmish[0] == "A":
                    new_hand_points += 1
                elif skirmish[0] == "B":
                    new_hand_points += 2
                elif skirmish[0] == "C":
                    new_hand_points += 3
            case "Z":
                new_skirmish_points += 6
                if skirmish[0] == "A":
                    new_hand_points += 2
                elif skirmish[0] == "B":
                    new_hand_points += 3
                elif skirmish[0] == "C":
                    new_hand_points += 1
    print(f"New hand points: {new_hand_points}")
    print(f"New skirmish points: {new_skirmish_points}")
    new_total = new_hand_points + new_skirmish_points
    print(f"New version totaL points: {new_total}")