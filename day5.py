def create_column_lists(file_path):
    with open(file_path, "r") as file:
        data = file.readlines()
        initial_config = data[:8]
        block_lines = []
        for line in initial_config:
            new_line = line.replace("    ", " [X] ")
            line_list = new_line.split()
            line_list = [crate[1] for crate in line_list]
            block_lines.append(line_list)
            print(line_list)

    block_columns = []
    for i in range(len(block_lines[0])):
        column = []
        for line in block_lines:
            column.append(line[i])
        column.reverse()
        column = [x for x in column if x != "X"]
        block_columns.append(column)
    print("\n-----------------------------\n")
    return block_columns


def get_instructions(file_path):
    with open(file_path, "r") as file:
        data = file.readlines()
        initial_instructions = data[10:]
        instructions = []
        for line in initial_instructions:
            new_line = line.split()
            for i in sorted([0, 2, 4], reverse=True):
                del new_line[i]
            new_line = [int(x) for x in new_line]
            instructions.append(new_line)
        return instructions


def task_1(crates_in_columns, instructions_sorted):
    crates = crates_in_columns
    print("Task 1 crates:")
    print(*crates, sep="\n")
    for instruction in instructions_sorted:
        quantity_to_move = instruction[0]
        move_from = instruction[1] - 1
        move_to = instruction[2] - 1
        while quantity_to_move > 0:
            crates[move_to].append(crates[move_from].pop())
            quantity_to_move -= 1
    return crates


def task_2(crates_in_columns, instructions_sorted):
    crates = crates_in_columns
    print("Task 2 crates:")
    print(*crates, sep="\n")
    for instruction in instructions_sorted:
        quantity_to_move = instruction[0]
        move_from = instruction[1] - 1
        move_to = instruction[2] - 1
        crates_moving = crates[move_from][-quantity_to_move:]
        crates[move_to].extend(crates_moving)
        del crates[move_from][-quantity_to_move:]

    print("\n-----------------------------\n")
    print(*crates, sep="\n")
    return crates


def get_result(task_outcome):
    result = []
    for column in task_outcome:
        result.append(column[-1])

    result = "".join(result)
    return result


crates_columns = create_column_lists("day5_input.txt")
crates_columns2 = create_column_lists("day5_input.txt")
instructions = get_instructions("day5_input.txt")
task1_result = get_result(task_1(crates_columns, instructions))
task2_result = get_result(task_2(crates_columns2, instructions))

print(f"Task 1: {task1_result} \nTask 2: {task2_result}")