
def read_file(file_path):
    with open(file_path, "r") as file:
        data = file.read()
        elves = data.split("\n\n")
        elves2 = [x.split("\n") for x in elves]
        for elf in elves2:
            if "" in elf:
                elf.remove("")

        elves_int = [list(map(int, x)) for x in elves2]
        # print(elves_int)
        calories = [sum(x) for x in elves_int]
        print(max(calories))

        calories.sort(reverse=True)
        print(calories[:3])
        print(sum(calories[:3]))
read_file("day1_input.txt")