with open("day4_input.txt", "r") as file:
  data = file.readlines()
  data = [x.strip() for x in data]

pairs = [x.split(",") for x in data]
new_list = []
for sublist in pairs:
  new_sublist = []
  for val in sublist:
    val = val.split("-")
    new_sublist.append(val)
  new_list.append(new_sublist)
# print(new_list)

overlapping_list = []
overlapping_list_2 = []
for pair in new_list:
  x1 = int(pair[0][0])
  y1 = int(pair[0][1])
  x2 = int(pair[1][0])
  y2 = int(pair[1][1])
  '''Case 1'''
  if (x1 <= x2 and y1 >= y2) or (x2 <= x1 and y2 >= y1):
    overlapping_list.append(pair)
  '''Case 2'''
  x = range(x1, y1 + 1)
  y = range(x2, y2 + 1)
  xs = set(x)
  if xs.intersection(y):
    overlapping_list_2.append(pair)

print(len(overlapping_list))
print(len(overlapping_list_2))
  