import string

lowercase_chars = list(string.ascii_lowercase)
uppercase_chars = list(string.ascii_uppercase)
all_chars = lowercase_chars + uppercase_chars
print(all_chars)


with open ("day3_input.txt", "r") as file:
  data = file.readlines()
  data = [x.strip() for x in data]

common_items = []
for rucksack in data:
  half_size = int(len(rucksack)/2)
  half_1 = list(rucksack[:half_size])
  half_2 = list(rucksack[half_size:])
  common_list = set(half_1).intersection(half_2)
  s = "".join(common_list)
  common_items.append(s)

score = 0
for item in common_items:
  value = all_chars.index(item) + 1
  score += value

print(score)

group_data = []
i = 0
while i < len(data):
  group = []
  for rucksack in data[i:(i+3)]:
    group.append(rucksack)
  group_data.append(group)
  i += 3

badges = []  
for group in group_data:
  common_list = set(group[0]).intersection(group[1], group[2])
  s = "".join(common_list)
  badges.append(s)

badge_score = 0
for item in badges:
  value = all_chars.index(item) + 1
  badge_score += value

print(badge_score)
  